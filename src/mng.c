/*
** mng.c for ftrace in /home/matthieu
** 
** Made by matthieu ciappara
** Login   <ciappa_m@epitech.net>
** 
** Started on  Sat Jun 16 14:28:35 2012 matthieu ciappara
** Last update Thu Jun 28 10:57:03 2012 matthieu ciappara
*/

#include <unistd.h>
#include <sys/types.h>
#include <sys/ptrace.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/wait.h>
#include <sys/user.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include "ezlist.h"
#include "fmacros.h"
#include "ftypes.h"
#include "fsyscall.h"
#include "ffunctions.h"

static void			opcode_to_call(unsigned long rip,
					       t_list calllist)
{
  t_list_i			*i;
  unsigned long			address;
  char				call[BUF_SZ];

  i = calllist;
  if (i && i->data)
    address = ((t_call *)(i->data))->address;
  while (i && address != rip)
    {
      i = i->next;
      if (i && address != rip && i->data)
	address = ((t_call *)(i->data))->address;
    }
  if (i && address == rip && ((t_call *)(i->data))->call)
    {
      write_graph(a_write, ((t_call *)(i->data))->call);
      printf(" => %s\n", ((t_call *)(i->data))->call);
    }
  else
    {
      snprintf(call, BUF_SZ, "0x%lX", rip);
      write_graph(a_write, call);
      printf("\n");
    }
}

void				mng_call(pid_t spoon, t_instr type,
					 t_list calllist)
{
  struct user_regs_struct	regs;
  t_lchar			next_instr;
  int				status;

  if (ptrace(PTRACE_SINGLESTEP, spoon, 0, 0) == -1)
    return ;
  if (wait4(spoon, &status, 0, 0) == -1)
    {
      fprintf(stderr, "Error : wait4 (%s, %d) : %s\n",
  	      __FILE__, __LINE__, strerror(errno));
      return ;
    }
  if (ptrace(PTRACE_GETREGS, spoon, 0, &regs) == -1)
    return ;
  next_instr.val = ptrace(PTRACE_PEEKDATA, spoon, regs.rip, 0);
  if (next_instr.val == -1)
    return ;
  if (type == callfar)
    printf("Call far : 0x%lX", regs.rip);
  else if (type == callnear)
    printf("Call near : 0x%lX", regs.rip);
  opcode_to_call(regs.rip, calllist);
}

static char			*opcode_to_syscall(unsigned long int opcode)
{
  char				*syscall;
  int				i;
  unsigned long int		op;

  i = 0;
  op = g_sys_ls[i].opcode;
  while (g_sys_ls[i].opcode != -1 &&
	 op != opcode)
    {
      i++;
      op = g_sys_ls[i].opcode;
    }
  if (!(syscall = strdup(g_sys_ls[i].syscall)))
    return (NULL);
  return (syscall);
}

static void			print_ret(long rax)
{
  if (rax >= 0)
    printf(" = %ld\n", rax);
  else
    printf(" = -1 (errno = %ld : %s)\n", -rax, strerror(-rax));
}

void				mng_syscall(pid_t spoon,
					    struct user_regs_struct *regs,
					    int *status)
{
  char				*call;

  if (ptrace(PTRACE_GETREGS, spoon, 0, regs) == -1)
    return ;
  if (!(call = opcode_to_syscall(regs->rax)))
    return ;
  printf("%lu : %s(args)", regs->rax, call);
  write_graph(a_write, call);
  free(call);
  if (ptrace(PTRACE_SINGLESTEP, spoon, 0, 0) == -1)
    return ;
  if (wait4(spoon, status, 0, 0) == -1)
    {
      fprintf(stderr, "Error : wait4 (%s, %d) : %s\n",
	      __FILE__, __LINE__, strerror(errno));
      return ;
    }
  (void)ptrace(PTRACE_GETREGS, spoon, 0, regs);
  print_ret(regs->rax);
}
