/*
** main.c for ftrace in /home/matthieu
** 
** Made by matthieu ciappara
** Login   <ciappa_m@epitech.net>
** 
** Started on  Thu May 10 10:00:42 2012 matthieu ciappara
** Last update Thu Jun 28 11:12:24 2012 matthieu ciappara
*/

#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "fmacros.h"
#include "ftypes.h"
#include "ffunctions.h"

static int	usage(char *prog)
{
  fprintf(stderr, "Usage :\n\t%s <-p pid>\n"
	  "\t%s <progname> [arguments] [...]\n", prog, prog);
  return (ERR_USAGE);
}

static char	**init_args(int argc, char **argv)
{
  char		**args;
  int		i;

  args = malloc(argc * sizeof(*args));
  if (!args)
    {
      fprintf(stderr, "Error : malloc (%s, %d)\n", __FILE__, __LINE__);
      return (NULL);
    }
  args[0] = argv[1];
  i = 1;
  while (i < argc)
    {
      args[i] = argv[i + 1];
      i++;
    }
  return (args);
}

static int	fork_and_trace(char **args, t_list calllist,
			       char *procname)
{
  pid_t		spoon;

  spoon = fork();
  if (spoon == -1)
    {
      fprintf(stderr, "Error : fork (%s, %d) : %s\n",
	      __FILE__, __LINE__, strerror(errno));
      free(args);
      return (ERR_FORK);
    }
  if (spoon == 0)
    return (child(args));
  free(args);
  return (father(spoon, TRACE_CHILD_PROCESS, calllist, procname));
}

int		main(int argc, char **argv)
{
  char		**args;
  int		existing_process;
  int		ret;
  t_list	calllist;

  calllist = NULL;
  if (argc < 2)
    return (usage(argv[0]));
  else if ((existing_process = strcmp(argv[1], "-p")) == 0 && argc >= 3)
    return (trace_existing_process(atoi(argv[2])));
  else if (existing_process == 0 && argc < 3)
    return (usage(argv[0]));
  if (!(args = init_args(argc, argv)))
    return (ERR_ARGS);
  if ((ret = elf_mng(argv[1], &calllist)))
    {
      free(args);
      return (ret);
    }
  return (fork_and_trace(args, calllist, argv[1]));
}
