/*
** free_call.c for ftrace in /home/matthieu
** 
** Made by matthieu ciappara
** Login   <ciappa_m@epitech.net>
** 
** Started on  Sat Jun 23 16:10:20 2012 matthieu ciappara
** Last update Mon Jun 25 14:48:26 2012 matthieu ciappara
*/

#include <stdlib.h>
#include "fmacros.h"
#include "ftypes.h"

void		free_call(void *data)
{
  t_call	*call;

  call = data;
  if (call->call != NULL)
    free(call->call);
  free(call);
}
