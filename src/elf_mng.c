/*
** elf_mng.c for ftrace in /home/matthieu
** 
** Made by matthieu ciappara
** Login   <ciappa_m@epitech.net>
** 
** Started on  Fri Jun 22 17:07:23 2012 matthieu ciappara
** Last update Thu Jun 28 11:15:42 2012 matthieu ciappara
*/

#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libelf.h>
#include "ezlist.h"
#include "fmacros.h"
#include "ftypes.h"
#include "felf.h"
#include "ffunctions.h"

int		elf_mng_s(int fd, t_list *calllist)
{
  Elf64_Ehdr	ehdr;
  Elf		*elf;

  if (!init_elf(&elf, fd))
    return (ERR_INIT_ELF);
  if (!check_elftype(&elf, &ehdr))
    return (ERR_CHECK_ELF);
  if (!get_calls(elf, calllist))
    return (ERR_GET_CALLS);
  (void)elf_end(elf);
  return (ERR_NONE);
}

int		elf_mng(char *prog, t_list *calllist)
{
  int		fd;
  char		*path;
  int		ret;

  if (!(path = which(prog)))
    {
      fprintf(stderr, "Error : Can't find %s in path\n", prog);
      return (ERR_WHICH);
    }
  if ((fd = open(path, O_RDONLY)) == -1)
    {
      fprintf(stderr, "Error : open (%s, %d) : %s\n",
	      __FILE__, __LINE__, strerror(errno));
      return (ERR_OPEN);
    }
  if (!strchr(path, '/'))
    free(path);
  ret = elf_mng_s(fd, calllist);
  (void)close(fd);
  return (ret);
}
