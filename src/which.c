/*
** which.c for ftrace in /home/matthieu
** 
** Made by matthieu ciappara
** Login   <ciappa_m@epitech.net>
** 
** Started on  Thu Jun 21 15:35:04 2012 matthieu ciappara
** Last update Mon Jun 25 14:52:16 2012 matthieu ciappara
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "fmacros.h"
#include "ftypes.h"

static char	*which_build(char *pos, int n, char *bin)
{
  int		size;
  char		*buf;

  size = n;
  if (pos[n - 1] != '/')
    size++;
  size += strlen(bin);
  if ((buf = malloc(sizeof(*buf) * (size + 1))) == NULL)
    {
      fprintf(stderr,
	      "Error : malloc (%s, %d) : Can't allocate memory",
	      __FILE__, __LINE__);
      return (NULL);
    }
  strncpy(buf, pos, n);
  if (pos[n - 1] != '/')
    {
      buf[n] = '/';
      strcpy(buf + n + 1, bin);
    }
  else
    strcpy(buf + n, bin);
  buf[size] = 0;
  return (buf);
}

static t_bool	which_check(const char *path)
{
  struct stat	istique;

  if (stat(path, &istique) == -1)
    return (false);
  if (S_ISDIR(istique.st_mode))
    return (false);
  if (access(path, X_OK) == -1)
    return (false);
  return (true);
}

char		*which(const char *bin)
{
  char		*buf;
  char		*path;
  char		*next;

  if (strchr(bin, '/'))
    return ((char *)bin);
  if (!(path = getenv("PATH")))
    return (NULL);
  while (*path)
    {
      next = strchr(path, ':');
      if (!next)
	next = path + strlen(path);
      buf = which_build(path, next - path, (char *)bin);
      if (buf)
	{
	  if (which_check(buf))
	    return (buf);
	  free(buf);
	}
      path = next;
      if (*path == ':')
	path++;
    }
  return (0);
}
