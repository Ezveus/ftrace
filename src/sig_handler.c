/*
** sig_handler.c for ftrace in /home/matthieu
** 
** Made by matthieu ciappara
** Login   <ciappa_m@epitech.net>
** 
** Started on  Sun May 13 13:46:05 2012 matthieu ciappara
** Last update Thu Jun 28 11:17:22 2012 matthieu ciappara
*/

#include <sys/types.h>
#include <sys/ptrace.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "ezlist.h"
#include "fmacros.h"
#include "ftypes.h"
#include "ffunctions.h"

static pid_t	g_spoon = 0;
static t_list	g_calllist = NULL;

void	sigint_ex_handler(int sigint)
{
  (void)sigint;
  if (ptrace(PTRACE_DETACH, g_spoon, 0, 0) == -1)
    fprintf(stderr, "\rError : ptrace (%s, %d) : %s\n",
	    __FILE__, __LINE__, strerror(errno));
  list_destroyf(g_calllist, free_call);
  write_graph(a_close, NULL);
  printf("\n=== Exited from ftrace ===\n");
  exit(ERR_NONE);
}

void	set_sig_handler(pid_t spoon, t_uint trace_type, t_list calllist)
{
  g_spoon = spoon;
  g_calllist = calllist;
  if (trace_type == TRACE_EXISTING_PROCESS)
    signal(SIGINT, sigint_ex_handler);
}
