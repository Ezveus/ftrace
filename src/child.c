/*
** child.c for ftrace in /home/matthieu
** 
** Made by matthieu ciappara
** Login   <ciappa_m@epitech.net>
** 
** Started on  Thu May 10 10:01:11 2012 matthieu ciappara
** Last update Mon Jun 25 14:28:36 2012 matthieu ciappara
*/

#include <unistd.h>
#include <sys/ptrace.h>

int	child(char **args)
{
  ptrace(PTRACE_TRACEME, 0, 0, 0);
  return (execvp(args[0], args));
}
