/*
** father.c for ftrace in /home/matthieu
** 
** Made by matthieu ciappara
** Login   <ciappa_m@epitech.net>
** 
** Started on  Thu May 10 09:58:45 2012 matthieu ciappara
** Last update Thu Jun 28 11:18:36 2012 matthieu ciappara
*/

#include <unistd.h>
#include <sys/types.h>
#include <sys/ptrace.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/wait.h>
#include <sys/user.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "ezlist.h"
#include "fmacros.h"
#include "ftypes.h"
#include "ffunctions.h"
#include "fsyscall.h"

static const t_uchar		op_syscall[] = {0x0f, 0x05};
static const t_uchar		op_int80[] = {0xcd, 0x80};
static const t_uchar		op_sysenter[] = {0x0f, 0x34};
static const t_uchar		op_callnear = 0xe8;
static const t_uchar		op_callfar = 0xff;

static int			check_exit(int status,
					   t_list calllist)
{
  if (WIFEXITED(status))
    {
      list_destroyf(calllist, free_call);
      printf("=== Exited with status %d ===\n", WEXITSTATUS(status));
      write_graph(a_close, NULL);
      return (WEXITSTATUS(status));
    }
  else if (WIFSIGNALED(status))
    {
      list_destroyf(calllist, free_call);
      printf("=== Exited with signal %d ===\n", WTERMSIG(status));
      write_graph(a_close, NULL);
      return (WTERMSIG(status));
    }
  return (-1);
}

static t_instr			cmp(t_lchar *next_instr)
{
  if (next_instr->tab[0] == op_syscall[0] &&
      next_instr->tab[1] == op_syscall[1])
    return (esyscall);
  else if (next_instr->tab[0] == op_int80[0] &&
	   next_instr->tab[1] == op_int80[1])
    return (esyscall);
  else if (next_instr->tab[0] == op_sysenter[0] &&
	   next_instr->tab[1] == op_sysenter[1])
    return (esyscall);
  else if (next_instr->tab[0] == op_callnear)
    return (callnear);
  else if (next_instr->tab[0] == op_callfar)
    return (callfar);
  return (none);
}

static t_instr			instr_type(pid_t spoon)
{
  struct user_regs_struct	regs;
  t_lchar			next_instr;

  next_instr.val = 0;
  if (ptrace(PTRACE_GETREGS, spoon, 0, &regs) == -1)
    return (0);
  next_instr.val = ptrace(PTRACE_PEEKDATA, spoon, regs.rip, 0);
  if (next_instr.val == -1)
    return (0);
  return (cmp(&next_instr));
}

static int			sub_father(pid_t spoon,
					   int *status,
					   t_list calllist)
{
  struct user_regs_struct	regs;
  int				ret;
  t_instr			type;

  type = instr_type(spoon);
  if (type == esyscall)
    mng_syscall(spoon, &regs, status);
  else if (IS_CALL(type))
    mng_call(spoon, type, calllist);
  if ((ret = check_exit(*status, calllist)) != -1)
    return (ret);
  return (-1);
}

int				father(pid_t spoon, t_uint trace_type,
				       t_list calllist,
				       char *procname)
{
  int				status;
  int				ret;

  set_sig_handler(spoon, trace_type, calllist);
  if (!write_graph(a_open, procname))
    return (ERR_OPEN);
  printf("=== Process ID %d ===\n", spoon);
  while (1)
    {
      ptrace(PTRACE_SINGLESTEP, spoon, 0, 0);
      if (wait4(spoon, &status, 0, 0) == -1)
	{
	  fprintf(stderr, "Error : wait4 (%s, %d) : %s\n",
		  __FILE__, __LINE__, strerror(errno));
	  return (ERR_WAIT);
	}
      if (WIFSIGNALED(status))
	{
	  printf("=== Exited with signal %d ===\n", WTERMSIG(status));
	  return (WTERMSIG(status));
	}
      if ((ret = sub_father(spoon, &status, calllist)) != -1)
	return (ret);
    }
  return (ERR_NONE);
}
