/*
** get_calls.c for ftrace in /home/matthieu
** 
** Made by matthieu ciappara
** Login   <ciappa_m@epitech.net>
** 
** Started on  Thu Jun 21 14:28:11 2012 matthieu ciappara
** Last update Mon Jun 25 15:28:58 2012 david ivanovic
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <libelf.h>
#include "ezlist.h"
#include "fmacros.h"
#include "ftypes.h"
#include "felf.h"

static t_bool	add_calls_in_list(Elf64_Sym *sym, char *sym_name,
				  t_list *list)
{
  t_call	*call;
  t_elem	*elem;

  if (!(call = malloc(sizeof(*call))))
    {
      fprintf(stderr, "Error : malloc (%s, %d) : Can't allocate memory\n",
	      __FILE__, __LINE__);
      return (false);
    }
  call->address = sym->st_value;
  if (!(call->call = strdup(sym_name)))
    {
      fprintf(stderr, "Error : strdup (%s, %d) : Can't allocate memory\n",
	      __FILE__, __LINE__);
      return (false);
    }
  if (!(elem = list_new(call)))
    {
      fprintf(stderr, "Error : list_new (%s, %d) : Can't allocate memory\n",
	      __FILE__, __LINE__);
      return (false);
    }
  list_add(list, elem);
  return (true);
}

static t_bool	next_sym(Elf *elf, Elf64_Shdr *shdr,
			 Elf64_Sym **sym, t_list *list)
{
  char		*sym_name;

  if (!(((*sym)->st_value == 0)
	|| (ELF64_ST_BIND((*sym)->st_info) == STB_LOCAL)
	|| (ELF64_ST_TYPE((*sym)->st_info) != STT_FUNC)))
    {
      if (!elf_str_ptr(elf, shdr, *sym, &sym_name))
	return (false);
      if (!add_calls_in_list(*sym, sym_name, list))
	return (false);
    }
  *sym += 1;
  return (true);
}

t_bool		get_calls(Elf *elf, t_list *list)
{
  Elf64_Shdr	*shdr;
  Elf_Scn	*scn;
  Elf_Data	*data;
  Elf64_Sym	*sym;
  Elf64_Sym	*sym_end;

  scn = NULL;
  while ((scn = elf_nextscn(elf, scn)) != NULL)
    {
      if (!elf_get_shdr(&shdr, scn))
	return (false);
      if (shdr->sh_type == SHT_SYMTAB)
	{
	  if (!elf_get_data(&data, scn))
	    return (false);
	  sym = (Elf64_Sym *)data->d_buf;
	  sym_end = (Elf64_Sym *)((char *)data->d_buf + data->d_size);
	  while (sym < sym_end)
	    if (!next_sym(elf, shdr, &sym, list))
	      return (false);
	}
    }
  return (true);
}
