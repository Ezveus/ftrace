/*
** lib_list.c for ftrace in /home/matthieu
** 
** Made by matthieu ciappara
** Login   <ciappa_m@epitech.net>
** 
** Started on  Fri Jun 22 16:37:02 2012 matthieu ciappara
** Last update Mon Jun 25 14:32:54 2012 matthieu ciappara
*/

#include <stdlib.h>
#include "ezlist.h"

t_list		list_add(t_list *begin, t_elem *elem)
{
  t_list_i	*tmp;

  if (!*begin)
    {
      *begin = elem;
      return (*begin);
    }
  tmp = list_end(*begin);
  tmp->next = elem;
  elem->prev = tmp;
  return (*begin);
}

void		list_insert(t_list_i *prev, t_elem *new)
{
  if (prev->next)
    {
      prev->next->prev = new;
      new->next = prev->next;
    }
  prev->next = new;
  new->prev = prev;
}

t_list		list_prepend(t_list *begin, t_elem *new)
{
  (*begin)->prev = new;
  new->next = *begin;
  return (new);
}

void		list_remove(t_list *begin, t_list_i *elem)
{
  t_list_i	*tmp;

  tmp = *begin;
  while (tmp && tmp != elem)
    tmp = tmp->next;
  if (tmp && tmp != *begin)
    {
      tmp = tmp->prev;
      tmp->next = elem->next;
      elem->prev = tmp;
    }
  else if (tmp == *begin)
    *begin = NULL;
}

t_elem		*list_new(void *data)
{
  t_elem	*elem;

  if (!(elem = malloc(sizeof(*elem))))
    return (NULL);
  elem->prev = NULL;
  elem->next = NULL;
  elem->data = data;
  return (elem);
}
