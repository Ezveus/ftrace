/*
** lib_list2.c for ftrace in /home/matthieu
** 
** Made by matthieu ciappara
** Login   <ciappa_m@epitech.net>
** 
** Started on  Fri Jun 22 16:38:05 2012 matthieu ciappara
** Last update Mon Jun 25 14:44:24 2012 matthieu ciappara
*/

#include <stdlib.h>
#include "ezlist.h"

size_t		list_count(t_list begin)
{
  size_t	i;
  t_list_i	*tmp;

  i = 0;
  tmp = begin;
  while (tmp)
    {
      tmp = tmp->next;
      i++;
    }
  return (i);
}

t_list_i	*list_goto(t_list begin, t_ezindex pos)
{
  t_list_i	*tmp;
  t_ezindex	i;

  tmp = begin;
  i = 0;
  while (tmp && i != pos)
    {
      i++;
      tmp = tmp->next;
    }
  return (tmp);
}

t_list_i	*list_end(t_list begin)
{
  t_list_i	*tmp;

  tmp = begin;
  while (tmp->next)
    tmp = tmp->next;
  return (tmp);
}

void		 list_destroy(t_list begin)
{
  list_destroyf(begin, free);
}

void		 list_destroyf(t_list begin, t_free_f free_f)
{
  if (begin && begin->next)
    list_destroyf(begin->next, free_f);
  if (begin && begin->data)
    free_f(begin->data);
  if (begin)
    free(begin);
}
