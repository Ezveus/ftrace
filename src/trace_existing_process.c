/*
** trace_existing_process.c for ftrace in /home/matthieu
** 
** Made by matthieu ciappara
** Login   <ciappa_m@epitech.net>
** 
** Started on  Thu May 10 11:04:26 2012 matthieu ciappara
** Last update Thu Jun 28 11:13:31 2012 matthieu ciappara
*/

#include <sys/types.h>
#include <sys/ptrace.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "fmacros.h"
#include "ftypes.h"
#include "ffunctions.h"

static void	epur_procname(char *procname)
{
  int		i;

  i = 0;
  while (procname[i] && i < BUF_SZ)
    {
      if (procname[i] == '\n')
	procname[i] = 0;
      i++;
    }
}

static t_bool	fill_procname(char *procname, pid_t spoon)
{
  FILE		*comm;
  char		commpath[BUF_SZ];

  snprintf(commpath, BUF_SZ, "/proc/%d/comm", spoon);
  if (!(comm = fopen(commpath, "r")))
    {
      fprintf(stderr, "Error : fopen (%s, %d) : %s\n",
	      __FILE__, __LINE__, strerror(errno));
      return (false);
    }
  if (!fgets(procname, BUF_SZ, comm))
    {
      fprintf(stderr, "Error : fgets (%s, %d) : Nothing read on %s\n",
	      __FILE__, __LINE__, commpath);
      return (false);
    }
  epur_procname(procname);
  (void)fclose(comm);
  return (true);
}

int		trace_existing_process(pid_t spoon)
{
  char		procname[BUF_SZ];
  t_list	calllist;

  calllist = NULL;
  if (!fill_procname((char *)procname, spoon))
    return (ERR_PROCNAME);
  (void)elf_mng(procname, &calllist);
  if (ptrace(PTRACE_ATTACH, spoon, 0, 0) == -1)
    {
      fprintf(stderr, "Error : ptrace (%s, %d) : %s\n",
	      __FILE__, __LINE__, strerror(errno));
      return (ERR_ATTACH);
    }
  return (father(spoon, TRACE_EXISTING_PROCESS, calllist, procname));
}
