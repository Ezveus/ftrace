/*
** write_graph.c for ftrace in /home/matthieu
** 
** Made by matthieu ciappara
** Login   <ciappa_m@epitech.net>
** 
** Started on  Wed Jun 27 16:55:57 2012 matthieu ciappara
** Last update Thu Jun 28 10:02:37 2012 matthieu ciappara
*/

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "fmacros.h"
#include "ftypes.h"

static void	epur_slashes(char *title)
{
  int		i;

  i = 0;
  while (title[i])
    {
      if (title[i] == '/' || title[i] == '.')
	title[i] = '_';
      i++;
    }
}

static t_bool	open_graph(FILE **graph, char *title)
{
  char		path[BUF_SZ];

  epur_slashes(title);
  snprintf(path, BUF_SZ, "%s.dot", title);
  if (!(*graph = fopen(path, "w")))
    {
      fprintf(stderr, "Error : fopen (%s, %d) : %s\n",
	      __FILE__, __LINE__, strerror(errno));
      return (false);
    }
  fprintf(*graph, "digraph %s {\n", title);
  return (true);
}

static t_bool	write_call(FILE *graph, char *call)
{
  static int	nb = 0;

  if (nb != 0)
    fprintf(graph, "-> ");
  nb++;
  fprintf(graph, "\"%s\"\n", call);
  return (true);
}

static t_bool	close_graph(FILE *graph)
{
  fprintf(graph, ";\n}\n");
  (void)fclose(graph);
  return (true);
}

t_bool		write_graph(t_action action, char *msg)
{
  static FILE	*graph = NULL;

  if (action == a_open)
    return (open_graph(&graph, msg));
  else if (action == a_write)
    return (write_call(graph, msg));
  return (close_graph(graph));
}
