/*
** ftr_elf.c for ftrace in /home/matthieu
** 
** Made by matthieu ciappara
** Login   <ciappa_m@epitech.net>
** 
** Started on  Thu Jun 21 14:28:11 2012 matthieu ciappara
** Last update Mon Jun 25 15:28:36 2012 david ivanovic
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <libelf.h>
#include "ezlist.h"
#include "fmacros.h"
#include "ftypes.h"

t_bool		init_elf(Elf **elf, const int fd)
{
  if (!elf_version(EV_CURRENT))
    {
      fprintf(stderr, "Error : elf_version (%s, %d) : %s\n",
	      __FILE__, __LINE__, elf_errmsg(elf_errno()));
      return (false);
    }
  if ((*elf = elf_begin(fd, ELF_C_READ, NULL)) == NULL)
    {
      fprintf(stderr, "Error : elf_begin (%s, %d) : %s\n",
	      __FILE__, __LINE__, elf_errmsg(elf_errno()));
      return (false);
    }
  return (true);
}

t_bool		check_elftype(Elf **elf, Elf64_Ehdr *ehdr)
{
  if ((ehdr = elf64_getehdr(*elf)) == NULL)
    {
      fprintf(stderr, "Error : elf64_getehdr (%s, %d) : %s\n",
	      __FILE__, __LINE__, elf_errmsg(elf_errno()));
      return (false);
    }
  if (ehdr->e_type != ET_EXEC)
    {
      fprintf(stderr, "Error : Programm is not executable\n");
      return (false);
   }
  if (ehdr->e_ident[EI_CLASS] == ELFCLASS32)
    {
      fprintf(stderr, "Error : Programm is not a 64bit executable.\n");
      return (false);
    }
  return (true);
}

t_bool		elf_get_shdr(Elf64_Shdr **shdr, Elf_Scn *scn)
{
  if ((*shdr = elf64_getshdr(scn)) == NULL)
    {
      fprintf(stderr, "SHDR error\n");
      return (false);
    }
  return (true);
}

t_bool		elf_get_data(Elf_Data **data, Elf_Scn *scn)
{
  if ((*data = elf_getdata(scn, NULL)) == NULL || (*data)->d_size == 0)
    {
      fprintf(stderr, "Section had no data\n");
      return (false);
    }
  return (true);
}

t_bool		elf_str_ptr(Elf *elf, Elf64_Shdr *shdr,
			    Elf64_Sym *sym, char **sym_name)
{
  *sym_name = elf_strptr(elf, shdr->sh_link, (size_t)sym->st_name);
  if (*sym_name == NULL)
    {
      fprintf(stderr, "%s\n", elf_errmsg(elf_errno()));
      return (false);
    }
  return (true);
}
