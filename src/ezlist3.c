/*
** lib_list3.c for ftrace in /home/matthieu
** 
** Made by matthieu ciappara
** Login   <ciappa_m@epitech.net>
** 
** Started on  Fri Jun 22 16:38:21 2012 matthieu ciappara
** Last update Mon Jun 25 14:46:51 2012 matthieu ciappara
*/

#include <stdlib.h>
#include "ezlist.h"

void		*list_to_tab(t_list begin)
{
  void		**tab;
  size_t	sz;
  t_list_i	*i;
  t_ezindex	j;

  sz = list_count(begin);
  if (!(tab = malloc(sz * sizeof(begin->data))))
      return (NULL);
  i = begin;
  j = 0;
  while (i)
    {
      tab[j] = i->data;
      i = i->next;
      j++;
    }
  return (tab);
}

t_list_i	*list_find(t_list begin, t_find_f func, void *ref)
{
  t_list_i	*i;

  i = begin;
  while (i && !func(ref, i->data))
    i = i->next;
  if (!i)
    return (NULL);
  return (i);
}
