#!/usr/bin/env perl
## Epitech.pm for project Epitech.pm
## 
## Made by Matthieu Ciappara
## Mail : <ciappam@gmail.com>
## 
## Started by  Matthieu Ciappara on Sun May  6 13:36:51 2012
## Last update Wed Jun 20 16:14:53 2012 matthieu ciappara
##

##
## Copyright (C) <2012> <Ciappara> <Matthieu>
## 
## This file is part of Epitech.pm
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public
## License as published by the Free Software Foundation; either
## version 3 of the License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public
## License along with this program. If not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
## or see <http://www.gnu.org/licenses/>.
##

=pod

=head1 NAME

Epitech - Define some usefull functions for Epitech students

=head1 SYNOPSIS

use Epitech;

my $teck_header = teck_getHeader('/*', '**', '*/', 'OUTPUT', 'PROJECT', 'current_dir');

=head1 EXPORTS

=cut

package Epitech;

use strict;
use warnings;

use Exporter;
use Time::localtime;

our @ISA = qw/Exporter/;
our @EXPORT = qw/&teck_getHeader/;

=pod

=head2 teck_getHeader

=over

=item Description

    It returns a string containing the Epitech header with given comments style, output, project and current directory

=item Input

    Top comment (C : "/*")
    Middle comment (C : "**")
    Bottom comment (C : "*/")
    Output (file name)
    Project (project name)
    Cwd (current working directory)

=item Output

    Epitech header as a string

=back

=cut

sub teck_getHeader($$$$$$) {
    my ($topcomment, $middlecomment, $bottomcomment,
	$output, $project, $pwd) = @_;
    my @header = ();
    my $email = 'undef@epitech.net';
    $email = $ENV{LOGIN} . '@epitech.net' if (defined($ENV{LOGIN}));
    my $author = 'undef';
    $author = $ENV{USER_FULLNAME} if (defined($ENV{USER_FULLNAME}));
    
    @header = (@header, $topcomment);
    @header = (@header, "$middlecomment $output for $project in $pwd");
    @header = (@header, "$middlecomment ");
    @header = (@header, "$middlecomment Made by $author");
    @header = (@header, "$middlecomment Login   <$email>");
    @header = (@header, "$middlecomment ");
    @header = (@header, "$middlecomment Started on  " . ctime() . " $author");
    @header = (@header, "$middlecomment Last update " . ctime() . " $author");
    @header = (@header, $bottomcomment);
    @header = (@header, "\n");
    return join("\n", @header);
}

=pod

=head1 BUGS

=head2 teck_getHeader

=over

    No verification is done on the content of parameters or environment.
    Consequently, it could have some blanks in the header.

=back

=head1 AUTHOR

    Original module by Matthieu Ciappara <ciappam@gmail.com>

=head1 LICENSE

    This module is free software, you may distribute it under the terms of
    the GNU GPL license

=cut

1;
