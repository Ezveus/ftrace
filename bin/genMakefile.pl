#!/usr/bin/env perl
## genMakefile for ftrace in /home/matthieu
## 
## Made by matthieu ciappara
## Login   <ciappa_m@epitech.net>
## 
## Started on  Sun May  6 12:46:00 2012 matthieu ciappara
## Last update Wed Jun 20 16:29:14 2012 matthieu ciappara
##

use strict;
use Getopt::Long qw(GetOptionsFromArray);
use bin::Epitech;

use constant E_NONE	=> 0;
use constant E_SYNTAX	=> 1;
use constant E_OPEN	=> 2;

my	$debug = 0;

sub print_usage() {
    say STDERR "Usage : $0 OPTIONS";
    say STDERR "--debug, -d : debug mode";
    say STDERR "--output, -o NAME : output name";
    say STDERR "--projname, -p NAME : project name";
    say STDERR "--srcdir, -s NAME : Name of the source directory";
    say STDERR "--includes, -I NAME [...] : Names of the include directories";
    say STDERR "--objdir, -j NAME : Name of the objects directory";
    say STDERR "--cflags FLAG [...] : C Compilator FLAGS";
    say STDERR "--cppflags FLAG [...] : C Preprocessor flags";
    say STDERR "--ldflags FLAG [...] : Linker flags";
    say STDERR "--ldlibs LIB [...] : Libraries to link with";
    say STDERR "--cc NAME : C Compilator";
}

sub write_file($$$$$$$$$$) {
    my ($makename, $projname, $srcdir, $hdrdirs_r, $objdir, $cflags_r, $cppflags_r, $ldflags_r, $ldlibs_r, $cc) = @_;
    my $status = 0;
    my $epitech_header = Epitech::teck_getHeader('##', '##', '##',
					'Makefile', 'ftrace',
					$ENV{HOME});
    my $srclistf = sub {
	my @srclist = <$srcdir/*.c>;
	map {$_ =~ s=$srcdir/==} @srclist;
	return join(" \\\n\t\t   ", @srclist);
    };

    my $srclist = &$srclistf;

    open(Makefile, '>', $makename) or $status = E_OPEN;
    if ($status) {
	print STDERR "Can't open $makename in write mode";
	return $status;
    }
    print Makefile $epitech_header;

    print Makefile "NAME\t\t = $projname\n\n";

    print Makefile "SOURCES\t\t = $srclist\n";
    print Makefile "\n";
    print Makefile "SRCDIR\t\t = $srcdir\n";
    print Makefile "HDRDIR\t\t = @$hdrdirs_r[0]\n";
    print Makefile "OBJDIR\t\t = $objdir\n";
    print Makefile "\n";
    print Makefile "SRC\t\t = \${addprefix \$(SRCDIR)/,\$(SOURCES)}\n";
    print Makefile "OBJ\t\t = \${addprefix \$(OBJDIR)/,\$(SOURCES:.c=.o)}\n";
    print Makefile "\n";
    print Makefile "CC\t\t = $cc\n";
    print Makefile "\n";
    print Makefile "CPPFLAGS\t+=", join(' ', @$cppflags_r), "\n";
    print Makefile "CFLAGS\t\t+= ", join(" \\\n\t\t   ", @$cflags_r), "\n";
    print Makefile "\n";
    print Makefile "LDFLAGS\t\t+=@$ldflags_r\n";
    print Makefile "LDLIBS\t\t+=@$ldlibs_r\n";
    print Makefile "\n";
    print Makefile "MKDIR\t\t = mkdir -p\n";
    print Makefile "RM\t\t = rm -f\n";
    print Makefile "\n";
    print Makefile "\$(NAME):\t\$(OBJDIR) \$(OBJ)\n";
    print Makefile "\t\$(CC) -o \$(NAME) \$(OBJ) \$(LDFLAGS) \$(LDLIBS)\n";
    print Makefile "\n";
    print Makefile "all:\t\t\$(NAME)\n";
    print Makefile "\n";
    print Makefile "\$(OBJDIR)/%.o:\t\$(SRCDIR)/%.c\n";
    print Makefile "\t\$(CC) -o \$\@ -c \$(CPPFLAGS) \$(CFLAGS) \$<\n";
    print Makefile "\n";
    print Makefile "\$(OBJDIR):\n";
    print Makefile "\t\$(MKDIR) \$(OBJDIR)\n";
    print Makefile "\n";
    print Makefile "clean:\n";
    print Makefile "\t\$(RM) \$(OBJ)\n";
    print Makefile "\n";
    print Makefile "fclean:\t\tclean\n";
    print Makefile "\t\$(RM) \$(NAME)\n";
    print Makefile "\n";
    print Makefile "ultraclean:\tfclean\n";
    print Makefile "\t\$(RM) -r \$(OBJDIR)\n";
    print Makefile "\t\$(RM) $makename\n";
    print Makefile "\n";
    print Makefile "re:\t\tfclean all\n";
    print Makefile "\n";
    print Makefile ".PHONY:\t\tall clean fclean re\n";
    close(Makefile);
    say STDOUT "$makename was written";
    return $status;
}

sub main() {
    my	$makename	= "Makefile";
    my	$projname	= "a.out";
    my	@srclist	= ("");
    my	$srcdir		= "src";
    my	@hdrdirs	= ("hdr");
    my	$objdir		= "obj";
    my	@cflags		= ("-I \$(HDRDIR)");
    my	@cppflags	= ("");
    my	@ldflags	= ("");
    my	@ldlibs		= ("");
    my	$cc		= "cc";

    my %args = (debug => \$debug,
		output => \$makename,
		projname => \$projname,
		srcdir => \$srcdir,
		includes => \@hdrdirs,
		objdir => \$objdir,
		cflags => \@cflags,
		cppflags => \@cppflags,
		ldflags => \@ldflags,
		ldlibs => \@ldlibs,
		cc => \$cc);
    my $status = 0;

    Getopt::Long::Configure("auto_help", "auto_version", "gnu_getopt");

    $status = GetOptionsFromArray(\@ARGV, \%args,
				  "debug|d!",
				  "output|o=s",
				  "projname|p=s",
				  "srcdir|s=s",
				  "includes|I=s",
				  "objdir|j=s",
				  "cflags=s",
				  "cppflags=s",
				  "ldflags=s",
				  "ldlibs=s",
				  "cc=s");
    if (!$status) {
	print STDERR "Syntax error\n";
	print_usage;
	return E_SYNTAX;
    }
    $status = write_file($makename, $projname, $srcdir, \@hdrdirs, $objdir, \@cflags, \@cppflags, \@ldflags, \@ldlibs, $cc);
    return $status;
}

my	$status = main;

exit $status;
