#!/usr/bin/env perl
## ftrace for syscall.pl in /home/matthieu
## 
## Made by matthieu ciappara
## Login   <ciappa_m@epitech.net>
## 
## Started on  Sat May  5 17:30:25 2012 matthieu ciappara
## Last update Mon Jun 25 14:51:24 2012 matthieu ciappara
##

use strict;
use warnings;

use Time::localtime;
use bin::Epitech;

unless (defined($ARGV[0])) {
    print(STDERR "Usage :\n\t$0 ASM_SYSCALL_FILE\n");
    exit(1);
}

my $inputname = $ARGV[0];
open(FILE, "<", $inputname)
    or die("Can't open $inputname : $!");

my $outputname = `uname -n` . "_" . `uname -s` . "_" . `uname -r` . "_" . `uname -m`;
$outputname =~ s/\n//g;

my $coutput = $outputname . ".c";
open(C, ">", $coutput)
    or die("Can't open $coutput : $!");

my $epitech_header = Epitech::teck_getHeader('/*', '**', '*/',
					     $coutput, 'ftrace', $ENV{HOME});

print(C $epitech_header);

print(C "#include <asm/unistd_64.h>\n");
print(C "#include <stdlib.h>\n");
print(C "#include \"fmacros.h\"\n\n");
print(C "#include \"ftypes.h\"\n\n");

print(C "const t_syscall	g_sys_ls[] =\n{\n");
while ((my $line = <FILE>)) {
    if ($line =~ /#define __NR/) {
	$line =~ s/#define __NR_(\w+).*$/{__NR_$1, "$1"},/;
	chomp($line);
	print(C "#ifdef __NR_$1\n");
	print(C "  $line\n");
	print(C "#endif\n");
    }
}
print(C "  {-1, \"\"}\n");
print(C "};\n");

close(C);
close(FILE);
print("$coutput was written\n");
