#!/usr/bin/env perl
## configure.pl for ftrace in /home/matthieu
## 
## Made by matthieu ciappara
## Login   <ciappa_m@epitech.net>
## 
## Started on  Sun May  6 12:06:47 2012 matthieu ciappara
## Last update Mon Jun 25 15:37:23 2012 david ivanovic
##

use strict;
use warnings;

use Getopt::Long qw(GetOptionsFromArray);

sub print_usage() {
    say STDERR "Usage : $0 OPTIONS";
    say STDERR "--debug, -d : debug mode";
    say STDERR "--output, -o NAME : output name";
    say STDERR "--projname, -p NAME : project name";
    say STDERR "--srcdir, -s NAME : Name of the source directory";
    say STDERR "--hdrdir, -h NAME [...] : Names of the include directories";
    say STDERR "--objdir, -j NAME : Name of the objects directory";
    say STDERR "--cflags FLAG [...] : C Compilator FLAGS";
    say STDERR "--cppflags FLAG [...] : C Preprocessor flags";
    say STDERR "--ldflags FLAG [...] : Linker flags";
    say STDERR "--ldlibs LIB [...] : Libraries to link with";
    say STDERR "--cc NAME : C Compilator";
}

sub main() {
    my	$makename	= "Makefile";
    my	$projname	= "ftrace";
    my	$srcdir		= "src";
    my	$hdrdir		= "hdr";
    my	$objdir		= "obj";
    my	@cflags		= ('-W', '-Wall', '-Werror', '-ansi', '-pedantic', '-Wfatal-errors', $ENV{CFLAGS});
    my	@cppflags	= ('-D_BSD_SOURCE', $ENV{CPPFLAGS});
    my	@ldflags	= ($ENV{LDFLAGS});
    my	@ldlibs		= ('-lelf', $ENV{LDLIBS});
    my	$cc		= "cc";

    my %args = (output => \$makename,
		projname => \$projname,
		srcdir => \$srcdir,
		hdrdir => \$hdrdir,
		objdir => \$objdir,
		cflags => \@cflags,
		cppflags => \@cppflags,
		ldflags => \@ldflags,
		ldlibs => \@ldlibs,
		cc => \$cc);
    my $status = 0;
    my $asm_syscall_file = "";

    Getopt::Long::Configure("auto_help", "auto_version", "gnu_getopt");

    $status = GetOptionsFromArray(\@ARGV, \%args,
				  "output|o=s",
				  "projname|p=s",
				  "srcdir|s=s",
				  "hdrdir|h=s",
				  "objdir|j=s",
				  "cflags=s",
				  "cppflags=s",
				  "ldflags=s",
				  "ldlibs=s",
				  "cc=s");
    if (!$status) {
	print_usage;
	return 1;
    }

    if (`uname -m`=~ /x86_64/) {
	$asm_syscall_file = "/usr/include/asm/unistd_64.h";
    } else {
	say STDERR "Unknown architecture";
	return 2;
    }

    my $output = `./bin/syscall.pl $asm_syscall_file`;
    print($output);
    chomp($output);
    $output =~ s/ was written//;
    system("mv", $output, $srcdir);
    my $epur_tab = sub {
	my @tmp = ();
	for (@_) {
	    @tmp = (@tmp, $_) if (defined($_));
	}
	return @tmp;
    };
    system("./bin/genMakefile.pl", "-o", $makename, "-p", $projname, "-s", $srcdir, "-I", $hdrdir, "-j", $objdir, "--cflags", join(' ', &$epur_tab(@cflags)), "--cppflags", join(' ', &$epur_tab(@cppflags)), "--ldflags", join(' ', &$epur_tab(@ldflags)), "--ldlibs", join(' ', &$epur_tab(@ldlibs)), "--cc", $cc);
    return 0;
}

my	$status = main;

exit $status;
