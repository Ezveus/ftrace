/*
** ffunctions.h for ffunctions.h in /home/ezveus
** 
** Made by matthieu ciappara
** Login   <ciappa_m@epitech.net>
** 
** Started on  Mon Jun 25 14:25:31 2012 matthieu ciappara
** Last update Thu Jun 28 10:51:02 2012 matthieu ciappara
*/

#ifndef FFUNCTIONS_H_
# define FFUNCTIONS_H_

int	father(pid_t spoon, t_uint trace_type, t_list calllist,
	       char *procname);
int	child(char **args);
int	trace_existing_process(pid_t pid);
void	set_sig_handler(pid_t spoon, t_uint trace_type, t_list calllist);
void	mng_call(pid_t spoon, t_instr type, t_list calllist);
char	*which(const char *bin);
int	elf_mng(char *prog, t_list *calllist);
void	free_call(void *data);
t_bool	write_graph(t_action action, char *msg);

#endif /* !FFUNCTIONS_H_ */
