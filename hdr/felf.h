/*
** felf.h for felf.h in /home/ezveus
** 
** Made by matthieu ciappara
** Login   <ciappa_m@epitech.net>
** 
** Started on  Mon Jun 25 14:25:45 2012 matthieu ciappara
** Last update Mon Jun 25 15:34:49 2012 david ivanovic
*/

#ifndef FELF_H_
# define FELF_H_

t_bool	init_elf(Elf **elf, const int fd);
t_bool	check_elftype(Elf **elf, Elf64_Ehdr *ehdr);
t_bool	elf_get_shdr(Elf64_Shdr **shdr, Elf_Scn *scn);
t_bool	elf_get_data(Elf_Data **data, Elf_Scn *scn);
t_bool	elf_str_ptr(Elf *elf, Elf64_Shdr *shdr,
		    Elf64_Sym *sym, char **sym_name);
t_bool	get_calls(Elf *elf, t_list *list);

#endif /* !FELF_H_ */
