/*
** ftypes.h for ftrace in /home/matthieu
** 
** Made by matthieu ciappara
** Login   <ciappa_m@epitech.net>
** 
** Started on  Mon Jun 25 13:43:46 2012 matthieu ciappara
** Last update Wed Jun 27 17:01:57 2012 matthieu ciappara
*/

#ifndef FTYPES_H_
# define FTYPES_H_

typedef unsigned int	t_uint;
typedef unsigned char	t_uchar;
typedef unsigned int	t_index;

typedef enum		e_bool
  {
    false		= 0,
    true		= !false
  }			t_bool;

typedef struct		s_syscall
{
  int			opcode;
  char			syscall[BUF_SZ];
}			t_syscall;

typedef union		u_lchar
{
  long			val;
  t_uchar		tab[sizeof(long)];
}			t_lchar;

typedef enum		e_instr
  {
    esyscall		= 1,
    callnear		= 2,
    none		= 3,
    callfar		= 4
  }			t_instr;

typedef struct		s_call
{
  unsigned long		address;
  char			*call;
}			t_call;

typedef enum		e_action
  {
    a_open,
    a_write,
    a_close
  }			t_action;

# ifndef EZLIST_H_
typedef struct s_elem	t_list_i;
typedef struct s_elem	*t_list;

typedef void (*t_free_f)(void *);
typedef t_bool (*t_find_f)(void *, void *);
# endif	 /* !EZLIST_H_ */

#endif	/* !FTYPES_H_ */
