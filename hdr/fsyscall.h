/*
** fsyscall.h for ftrace in /home/matthieu
** 
** Made by matthieu ciappara
** Login   <ciappa_m@epitech.net>
** 
** Started on  Mon Jun 25 14:24:31 2012 matthieu ciappara
** Last update Mon Jun 25 15:35:21 2012 david ivanovic
*/

#ifndef	FSYSCALL_H_
# define FSYSCALL_H_

extern t_syscall	g_sys_ls[];

void	mng_syscall(pid_t spoon, struct user_regs_struct *regs, int *status);

#endif /* !FSYSCALL_H_ */
