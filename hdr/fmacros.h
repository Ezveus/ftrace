/*
** fmacros.h for ftrace in /home/matthieu
** 
** Made by matthieu ciappara
** Login   <ciappa_m@epitech.net>
** 
** Started on  Mon Jun 25 14:23:14 2012 matthieu ciappara
** Last update Thu Jun 28 11:18:22 2012 matthieu ciappara
*/

#ifndef FMACROS_H_
# define FMACROS_H_

# define	BUF_SZ			(512)
# define	TRACE_EXISTING_PROCESS	0
# define	TRACE_CHILD_PROCESS	1

# define	ERR_NONE		0
# define	ERR_USAGE		1
# define	ERR_ARGS		2
# define	ERR_FORK		3
# define	ERR_ATTACH		4
# define	ERR_OPEN		5
# define	ERR_WAIT		6
# define	ERR_WHICH		7
# define	ERR_INIT_ELF		8
# define	ERR_CHECK_ELF		9
# define	ERR_GET_CALLS		10
# define	ERR_PROCNAME		11

# define	IS_CALL(type)		(type % 2 == 0 ? true : false)

#endif /* !FMACROS_H_ */
