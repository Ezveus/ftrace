/*
** ezlist.h for ezlist in /home/ezveus
** 
** Made by matthieu ciappara
** Login   <ciappa_m@epitech.net>
** 
** Started on  Mon Jun 25 14:16:32 2012 matthieu ciappara
** Last update Mon Jun 25 16:09:14 2012 david ivanovic
*/

#ifndef EZLIST_H_
# define EZLIST_H_

enum			e_ezbool
  {
    ezfalse		= 0,
    eztrue		= !ezfalse
  };

struct			s_elem
{
  struct s_elem		*next;
  void			*data;
  struct s_elem		*prev;
};

typedef unsigned int	t_ezindex;

typedef struct s_elem	t_elem;
typedef struct s_elem	t_list_i;
typedef struct s_elem	*t_list;
typedef enum e_ezbool	t_ezbool;

typedef void (*t_free_f)(void *);
typedef t_ezbool (*t_find_f)(void *, void *);

t_list		list_add(t_list *, t_elem *);
void		list_insert(t_list_i *, t_elem *);
t_list		list_prepend(t_list *, t_elem *);
void		list_remove(t_list *, t_list_i *);
t_elem		*list_new(void *);
size_t		list_count(t_list);
t_list_i	*list_goto(t_list, t_ezindex);
t_list_i	*list_end(t_list);
void		list_destroy(t_list);
void		list_destroyf(t_list, t_free_f);
void		*list_to_tab(t_list);
t_list_i	*list_find(t_list, t_find_f, void *);

#endif /* !EZLIST_H_ */
